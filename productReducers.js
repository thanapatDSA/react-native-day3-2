
export default (state = [], action) => {
    state=[{
        pic: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSqD4kSXL_VAaHrhJDyYd8PeqiJTkJwwpiK52IDNwxJss-350qq',
        name: 'test'
    }]
    switch (action.type) {
        case 'ADD_ITEM':
            return [...state, {
                pic: action.pic,
                name: action.name
            }]
        case 'REMOVE_ITEM':
            return state.filter((item, index) => index !== action.index)
        default:
            return state
    }
}

