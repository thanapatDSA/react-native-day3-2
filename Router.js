import React, { Component } from 'react'
import { Route, NativeRouter, Switch, Redirect } from 'react-router-native'
import { Provider } from 'react-redux'
import store from './AppStore'
import LoginPage from './Container/login'
import ListPage from './Container/list'
import ListProduct from './Container/listProduct'
import ProfilePage from './Container/profile'
import ProductPage from './Container/product'
import editProfile from './Container/editProfile'
import editProduct from './Container/editProduct'
import addProduct from './Container/addProduct'
import test from './testFlex'


class Router extends Component {
    render() {
        return (
            <Provider store={store}>
                <NativeRouter>
                    <Switch>
                        <Route exact path="/login" component={LoginPage} />
                        <Route exact path="/list" component={ListPage} />
                        <Route exact path="/profile" component={ProfilePage} />
                        <Route exact path="/listproduct" component={ListProduct} />
                        <Route exact path="/product" component={ProductPage} />
                        <Route exact path="/editprofile" component={editProfile} />
                        <Route exact path="/editproduct" component={editProduct} />
                        <Route exact path="/addproduct" component={addProduct} />
                        <Route exact path="/test" component={test} />
                        <Redirect to='/list' />
                    </Switch>
                </NativeRouter>
            </Provider>

        )
    }
}
export default Router