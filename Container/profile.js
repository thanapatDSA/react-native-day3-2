
import React, { Component } from 'react';
import { Image, StyleSheet, Text, View, Alert, TextInput, TouchableOpacity, ScrollView } from 'react-native';
import { NavBar, Icon, Card, Button, WingBlank } from '@ant-design/react-native';
import { connect } from 'react-redux'
import styles from '../style'

class profile extends Component {
    goToList = () => {
        this.props.history.push('./list')
    }
    goToEditProfile = () => {
        this.props.history.push('./editprofile')
    }
    render() {
        const { profile } = this.props
        return (
            <View style={{ flex: 1 }}>
                <View style={styles.header}>
                    <View style={{ flex: 1, width: 50, height: 50, backgroundColor: 'powderblue' }} >
                        <Button style={{ height: 50 }} onPress={() => { this.goToList() }}>Back</Button>
                    </View>
                    <View style={{ flex: 2, width: 50, height: 50, backgroundColor: 'powderblue', alignItems: 'center' }} >
                        <Text style={{ fontSize: 20, padding: 12 }}>ProfilePage</Text>
                    </View>
                </View>

                <View style={styles.content}>
                    <ScrollView>
                        <Text style={{ fontSize: 10, padding: 5 }} >Username</Text>
                        <Text style={{ fontSize: 20, padding: 15 }} >{profile[0].username}</Text>
                        <Text style={{ fontSize: 10, padding: 5 }} >First name</Text>
                        <Text style={{ fontSize: 20, padding: 15 }} >{profile[0].firstname}</Text>
                        <Text style={{ fontSize: 10, padding: 5 }} >Last name</Text>
                        <Text style={{ fontSize: 20, padding: 15 }} >{profile[0].lastname}</Text>
                    </ScrollView>

                </View>
                <View style={styles.footer}>
                    <View style={{ height: 50, backgroundColor: 'powderblue', flexDirection: 'row' }}>
                        <View style={{ flex: 1 }}>
                            <Button onPress={() => { this.goToEditProfile() }}>Edit</Button>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}

  const mapStateToProps = (state) => {
    return {
        profile: state.profile
    }
}

export default connect(mapStateToProps)(profile)