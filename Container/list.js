
import React, { Component } from 'react';
import { Image, StyleSheet, Text, View, Alert, TextInput, TouchableOpacity, ScrollView } from 'react-native';
import { NavBar, Icon, Card, Button, WingBlank } from '@ant-design/react-native';
import { connect } from 'react-redux'
import ListProduct from './listProduct'
import styles from '../style'


class list extends Component {
    goToProfile = () => {
        this.props.history.push('./profile')
    }
    goToProduct = () => {
        this.props.history.push('./product')
      }

    goToAddProduct = () => {
        this.props.history.push('./addproduct')
    }
    goToProduct = () => {
        this.props.history.push('./product')
    }
    render() {
        const { profile,product } = this.props
        
        return (
            <View style={{ flex: 1 }}>
                <View style={styles.header}>
                    <View style={{ flex: 1, width: 50, height: 50, backgroundColor: 'powderblue' }} >
                        <Button style={{ height: 50 }}>Logout</Button>
                    </View>
                    <View style={{ flex: 2, width: 50, height: 50, backgroundColor: 'powderblue', alignItems: 'center' }} >
                        <Text style={{ fontSize: 20, padding: 12 }}>List</Text>
                    </View>
                </View>

                <View style={styles.content}>
                    <ScrollView>
                    <Card>
        <Card.Header
          title="This is title"
          thumbStyle={{ width: 30, height: 30 }}
          thumb="https://gw.alipayobjects.com/zos/rmsportal/MRhHctKOineMbKAZslML.jpg"
          extra="this is extra"
        />
        <Card.Body>
          <View style={{ height: 42 }}>
            <Text style={{ marginLeft: 16 }}>Card Content</Text>
          </View>
          <Button onPress={() => { this.goToProduct() }}>View product</Button>
        </Card.Body>
        <Card.Footer
          content="footer content"
          extra="footer extra content"
        />
      </Card>
                    </ScrollView>

                </View>
                <View style={styles.footer}>
                    <View style={{ height: 50, backgroundColor: 'powderblue', flexDirection: 'row' }}>
                        <View style={{ flex: 1}}>
                            <Button onPress={() => { this.goToAddProduct() }}>ADD Product</Button>
                        </View>
                        <View style={{ flex: 1}}>
                            <Button onPress={() => { this.goToProfile() }}>Profile</Button>
                        </View>
                    </View>
                </View>
            </View>

        )
    }

}

  const mapStateToProps = (state) => {
    return {
        profile: state.profile,
        product: state.product
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addProfile: (username, firstname, lastname) => {
            dispatch({
                type: 'ADD_ITEM',
                username: username,
                firstname: firstname,
                lastname: lastname
            })
        },
        addItem: (name, pic) => {
            dispatch({
                type: 'ADD_ITEM',
                name: name,
                pic: pic
            })
        }
    }
}


export default connect(mapStateToProps,mapDispatchToProps)(list)