import React, { Component } from 'react';
import { Image, StyleSheet, Text, View, Alert, TextInput, TouchableOpacity, ScrollView } from 'react-native';
import { NavBar, Icon, Card, Button, WingBlank } from '@ant-design/react-native';
import { connect } from 'react-redux'

class ListProduct extends Component {
  goToProduct = () => {
    this.props.history.push('./product')
  }
  render() {
    const { product } = this.props
    console.log('product:', product);
    console.log('product len:', product.length - 1);
    for (let i = 0; i < product.length - 1; i++) {
      console.log('i len:', i);
      console.log('productItem:', product[i]);
    }
    return (

      <Card>
        <Card.Header
          title="This is title"
          thumbStyle={{ width: 30, height: 30 }}
          thumb="https://gw.alipayobjects.com/zos/rmsportal/MRhHctKOineMbKAZslML.jpg"
          extra="this is extra"
        />
        <Card.Body>
          <View style={{ height: 42 }}>
            <Text style={{ marginLeft: 16 }}>Card Content</Text>
          </View>
          <Button onPress={() => { this.goToProduct() }}>View product</Button>
        </Card.Body>
        <Card.Footer
          content="footer content"
          extra="footer extra content"
        />
      </Card>
    )
  }
}



const mapStateToProps = (state) => {
  return {
    product: state.product
  }
}

export default connect(mapStateToProps)(ListProduct)