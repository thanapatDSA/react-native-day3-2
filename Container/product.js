
import React, { Component } from 'react';
import { Image, StyleSheet, Text, View, Alert, TextInput, TouchableOpacity, ScrollView } from 'react-native';
import { NavBar, Icon, Card, Button, WingBlank } from '@ant-design/react-native';
import { connect } from 'react-redux'
import styles from '../style'

class product extends Component {
    state = {
        pic: 'https://www.almau.edu.kz/img/no_image.png'
    }
    goToList = () => {
        this.props.history.push('./list')
    }
    goToEditProduct = () => {
        this.props.history.push('./editproduct')
    }
    render() {
        const { product } = this.props
        return (
            <View style={{ flex: 1 }}>
                <View style={styles.header}>
                    <View style={{ flex: 1, width: 50, height: 50, backgroundColor: 'powderblue' }} >
                        <Button style={{ height: 50 }} onPress={() => { this.goToList() }}>Back</Button>
                    </View>
                    <View style={{ flex: 2, width: 50, height: 50, backgroundColor: 'powderblue', alignItems: 'center' }} >
                        <Text style={{ fontSize: 20, padding: 12 }}>ProductPage</Text>
                    </View>
                </View>

                <View style={styles.content}>
                    <ScrollView>
                        <Image style={{ width: 300, height: 200 }} source={{ uri: product[product.length-1].pic}} />
                        <Text style={{ fontSize: 15, padding: 30 }} >{product[product.length-1].name}</Text>
                    </ScrollView>

                </View>
                <View style={styles.footer}>
                    <View style={{ height: 50, backgroundColor: 'powderblue', flexDirection: 'row' }}>
                        <View style={{ flex: 1 }}>
                            <Button onPress={() => { this.goToEditProduct() }}>Edit</Button>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        product: state.product
    }
}

export default connect(mapStateToProps)(product)