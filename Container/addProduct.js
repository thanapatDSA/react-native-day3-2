import React, { Component } from 'react';
import { Image, StyleSheet, Text, View, Alert, TextInput, TouchableOpacity, ScrollView } from 'react-native';
import { NavBar, Icon, Card, Button, WingBlank, InputItem } from '@ant-design/react-native';
import { connect } from 'react-redux'
import styles from '../style'

class addProduct extends Component {
    state = {
        name: '',
        pic: 'https://www.almau.edu.kz/img/no_image.png'
    }
    goToList = () => {
        this.props.history.push('./list')
    }
    render() {
        const { product, addItem } = this.props

        return (
            <View style={{ flex: 1 }}>
                <View style={styles.header}>
                    <View style={{ flex: 1, width: 50, height: 50, backgroundColor: 'powderblue' }} >
                        <Button style={{ height: 50 }} onPress={() => { this.goToList() }}>Back</Button>
                    </View>
                    <View style={{ flex: 2, width: 50, height: 50, backgroundColor: 'powderblue', alignItems: 'center' }} >
                        <Text style={{ fontSize: 20, padding: 12 }}>AddProductPage</Text>
                    </View>
                </View>

                <View style={styles.content}>
                    <ScrollView>
                        <InputItem
                            clear
                            onChange={() => { }}
                            placeholder="Image"
                        />
                        <InputItem
                            clear
                            onChangeText={value => { this.setState({ name: value }) }}
                            placeholder="name"
                        />
                        <Text>
                            Lastest Item name is: {product[product.length-1].name}
                        </Text>
                    </ScrollView>

                </View>
                <View style={styles.footer}>
                    <View style={{ height: 50, backgroundColor: 'powderblue', flexDirection: 'row' }}>
                        <View style={{ flex: 1 }}>
                            <Button onPress={() => { addItem(this.state.name, this.state.pic) }}>SAVE</Button>
                        </View>
                    </View>
                </View>
            </View>

        )
    }

}

const mapStateToProps = (state) => {
    return {
        product: state.product
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addItem: (name, pic) => {
            dispatch({
                type: 'ADD_ITEM',
                name: name,
                pic: pic
            })
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(addProduct)